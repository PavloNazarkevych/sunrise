package com.nazarkevych.teamvoy;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Locale;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder> implements Filterable {
    private AdapterView.OnItemClickListener onItemClickListener;
    private LayoutInflater layoutInflater;

    private ArrayList<SimpleText> wordslist;
    private ArrayList<SimpleText> arraylist;

    public MyAdapter(Context context, AdapterView.OnItemClickListener onItemClickListener, ArrayList<SimpleText> wordslist) {
        layoutInflater = LayoutInflater.from(context);
        this.wordslist = wordslist;
        this.onItemClickListener = onItemClickListener;
        this.arraylist = wordslist;
    }

    // Filter Class
    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        wordslist.clear();
        if (charText.length() == 0) {
            wordslist.addAll(arraylist);
        } else {
            for (SimpleText wp : arraylist) {
                if (wp.getText().toLowerCase(Locale.getDefault()).contains(charText)) {
                    wordslist.add(wp);
                }
            }
        }
        notifyDataSetChanged();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {

                String charString = charSequence.toString();

                if (charString.isEmpty()) {

                    arraylist = wordslist;
                } else {

                    ArrayList<SimpleText> filteredList = new ArrayList<>();

                    for (SimpleText androidVersion : wordslist) {

                        if (androidVersion.getText().toLowerCase().contains(charString)) {

                            filteredList.add(androidVersion);
                        }
                    }

                    arraylist = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = arraylist;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                arraylist = (ArrayList<SimpleText>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }


    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        public TextView sword;
        public MyViewHolder(View view){
            super(view);
            sword=(TextView)view.findViewById(R.id.recycleTxt);
            sword.setOnClickListener(this);
        }


        @Override
        public void onClick(View view) {
            onItemClickListener.onItemClick(null, view, getAdapterPosition(), view.getId());
        }
    }

    public MyAdapter(ArrayList<SimpleText> wordslist){
        this.wordslist=wordslist;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView= layoutInflater.from(parent.getContext()).inflate(R.layout.item_recycle,parent,false);
        MyViewHolder vh=new MyViewHolder(itemView);
        return vh;
        //return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        SimpleText sample = arraylist.get(position);
        holder.sword.setText(sample.getText());
    }

    @Override
    public int getItemCount() {
        return arraylist.size();
    }
}
