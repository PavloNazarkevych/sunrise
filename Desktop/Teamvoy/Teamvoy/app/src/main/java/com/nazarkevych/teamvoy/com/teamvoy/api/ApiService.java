package com.nazarkevych.teamvoy.com.teamvoy.api;

import com.nazarkevych.teamvoy.Example;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ApiService {

    @GET("/json")
    Call<Example> getData(@Query("lat") String latitude, @Query("lng") String longitude);
}
