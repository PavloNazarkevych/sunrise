package com.nazarkevych.teamvoy;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;
import com.nazarkevych.teamvoy.com.teamvoy.api.App;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity implements AdapterView.OnItemClickListener{

    String [] city = {"L'viv", "Berlin", "London", "Dnipro", "Milan","Moscow", "Tokyo", "Odessa"};


    List<LatLng> ll;
    ArrayList<SimpleText> list;

    private static final int MY_PERMISSION_REQUEST_LOCATION = 1;


    EditText txtLocation;
    TextView txMyLocation, txtSunset, txtSunrise;
    Button btnGetSunriseSunset, btnMyLocation;
    ImageButton btnAllList;
    public RecyclerView rv;
    private RecyclerView.LayoutManager mLayoutManager;
    private MyAdapter mAdapter;

    LocationManager locationManager;
    Location location;

    String latitude;
    String longitude;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initView();
        initAdapterAndList();

        try {
            Response response = App.getApi().getData("00", "00").execute();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }



    private void initAdapterAndList(){
        rv = (RecyclerView) findViewById(R.id.rv);
        rv.setVisibility(View.INVISIBLE);
        list = new ArrayList<>();

        for(int i = 0; i<city.length; i++) {
            list.add(new SimpleText(city[i]));
        }

        mLayoutManager = new LinearLayoutManager(this);
        rv.setLayoutManager(mLayoutManager);
        mAdapter = new MyAdapter(MainActivity.this, this, list);
        rv.setAdapter(mAdapter);
    }

    private void initView(){
        btnAllList = (ImageButton) findViewById(R.id.btnAllList);
        btnGetSunriseSunset = (Button) findViewById(R.id.btnGetInfo);
        txMyLocation = (TextView) findViewById(R.id.txtView);
        txtLocation = (EditText) findViewById(R.id.txtLocation);
        btnMyLocation = (Button) findViewById(R.id.btnMyLocation);
        txtSunrise = (TextView) findViewById(R.id.txtSunrise);
        txtSunset = (TextView) findViewById(R.id.txtSunset);
        btnMyLocation.setOnClickListener(mGetLocationClickListener);
        btnGetSunriseSunset.setOnClickListener(mGetSunriseSunset);
        btnAllList.setOnClickListener(mAllListListener);
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
        txtLocation.setText(list.get(position).getText());
    }

    private String getMyLocation(double lat, double lon){
        String curSity = "";

        Geocoder geocoder = new Geocoder(MainActivity.this, Locale.getDefault());

        List<Address> addressList;
        try {
            addressList = geocoder.getFromLocation(lat,lon,2);
            if(addressList.size() > 0) {
                curSity = addressList.get(0).getLocality();
            }
        }catch (Exception e) {
            e.printStackTrace();
        }
        return curSity;
    }


    final View.OnClickListener mGetLocationClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if(ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION)!= PackageManager.PERMISSION_GRANTED) {
                if(ActivityCompat.shouldShowRequestPermissionRationale(MainActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION)) {
                    ActivityCompat.requestPermissions(MainActivity.this, new String[] {Manifest.permission.ACCESS_COARSE_LOCATION}, MY_PERMISSION_REQUEST_LOCATION);
                } else {
                    ActivityCompat.requestPermissions(MainActivity.this, new String[] {Manifest.permission.ACCESS_COARSE_LOCATION}, MY_PERMISSION_REQUEST_LOCATION);
                }
            } else {
                locationManager =(LocationManager) getSystemService(Context.LOCATION_SERVICE);
                location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                try {
                    txtLocation.setText(getMyLocation(location.getLatitude(),location.getLongitude()));
                    latitude = Double.toString(location.getLatitude());
                    longitude = Double.toString(location.getLongitude());
                } catch (Exception e){
                    e.printStackTrace();

                    Toast.makeText(MainActivity.this, "Not found",Toast.LENGTH_SHORT).show();
                }
            }
        }
    };

    final View.OnClickListener mAllListListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if(rv.getVisibility() == View.VISIBLE){
                rv.setVisibility(View.INVISIBLE);
            } else rv.setVisibility(View.VISIBLE);

        }
    };

    final View.OnClickListener mGetSunriseSunset = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if(txtLocation.getText().toString().equals("")) {
                Toast.makeText(MainActivity.this, "Please set your location", Toast.LENGTH_SHORT).show();
            } else {

                InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(txtLocation.getWindowToken(), 0);
                placeLatLng(txtLocation.getText().toString());

                if(!latitude.equals("") || !longitude.equals("")) {
                    getSunriseSunset();
                } else {
                    Toast.makeText(MainActivity.this, "Location not found", Toast.LENGTH_SHORT).show();
                }
            }
        }
    };

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode){
            case MY_PERMISSION_REQUEST_LOCATION:
                if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if(ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED){
                        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
                        location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                        try {
                            txMyLocation.setText(getMyLocation(location.getLatitude(),location.getLongitude()));
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
        }
    }

    public void getSunriseSunset(){
        App.getApi().getData(latitude,longitude).enqueue(new Callback<Example>() {
            @Override
            public void onResponse(Call<Example> call, Response<Example> response) {
                txtSunrise.setText(convertTime(response.body().getResults().getSunrise()));
                txtSunset.setText(convertTime(response.body().getResults().getSunset()));
            }

            @Override
            public void onFailure(Call<Example> call, Throwable t) {
                Toast.makeText(MainActivity.this, "An error occurred during networking", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private String convertTime(String time) {
        SimpleDateFormat df = new SimpleDateFormat("hh:mm:ss a", Locale.ENGLISH);
        df.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date date = null;
        try {
            date = df.parse(time);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        df.setTimeZone(TimeZone.getDefault());
        String formattedDate = df.format(date);

        SimpleDateFormat displayFormat = new SimpleDateFormat("HH:mm:ss");
        SimpleDateFormat parseFormat = new SimpleDateFormat("hh:mm:ss a");
        Date dateTwo = null;
        try {
            dateTwo = parseFormat.parse(formattedDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        formattedDate = displayFormat.format(dateTwo);
        return formattedDate;
    }

    private void placeLatLng(String place){
        if(Geocoder.isPresent()){
            try {
                Geocoder gc = new Geocoder(this);
                List<Address> addresses= gc.getFromLocationName(place, 5); // get the found Address Objects
                ll = new ArrayList<LatLng>(addresses.size()); // A list to save the coordinates if they are available

                for(Address a : addresses){
                    if(a.hasLatitude() && a.hasLongitude()){
                        ll.add(new LatLng(a.getLatitude(), a.getLongitude()));
                        latitude = Double.toString(a.getLatitude());
                        longitude = Double.toString(a.getLongitude());
                    } else {
                        latitude = "";
                        longitude = "";
                    }
                }

            } catch (IOException e) {
                // handle the exception
            }
        }
    }
}