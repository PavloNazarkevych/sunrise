package com.nazarkevych.teamvoy.com.teamvoy.api;

import android.app.Application;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class App extends Application {

    private static ApiService apiService;
    private Retrofit retrofit;

    @Override
    public void onCreate() {
        super.onCreate();

        retrofit = new Retrofit.Builder()
                .baseUrl("https://api.sunrise-sunset.org") //Базовая часть адреса
                .addConverterFactory(GsonConverterFactory.create()) //Конвертер, необходимый для преобразования JSON'а в объекты
                .build();
        apiService = retrofit.create(ApiService.class); //Создаем объект, при помощи которого будем выполнять запросы
    }

    public static ApiService getApi() {
        return apiService;
    }
}
